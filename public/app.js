// 🛑 Replace with your Firebase Config.  

console.log("### Começa aqui");
var img_original_URL;
var userid; 

// Firebase Config
// var firebaseConfig0 = {
//  apiKey: "**********",
//   authDomain: "**********",
//   databaseURL: "**********",
//   projectId: "**********",
//   storageBucket: "**********",
//   messagingSenderId: "**********",
//   appId: "**********",
//   measurementId: "**********"
// };
var firebaseConfig = {
    apiKey: "AIzaSyDVQbP7JUXkCh9q7GO_-av3Wo3UFcnGPUA",
    authDomain: "makersregistration-b6f11.firebaseapp.com",
    databaseURL: "https://makersregistration-b6f11-default-rtdb.firebaseio.com",
    projectId: "makersregistration-b6f11",
    storageBucket: "makersregistration-b6f11.appspot.com",
    messagingSenderId: "159784800375",
    appId: "1:159784800375:web:b80d7a3d05ca6651e23672"
}

// Initialize Firebase
firebase.initializeApp(firebaseConfig); // Pelo console do firebase
// const app = initializeApp(firebaseConfig); // Pelo console do firebase: 

// -------------------------------------
// Upload Profile Picture With Authentication
// -------------------------------------

// DOM Elements

const email = document.getElementById('email'),
  pword = document.getElementById('pword'),
  fileUploader = document.getElementById('fileUploader'),
  img = document.getElementById('img')
  // 2023-01-18
  img_modificada = document.getElementById('img_modificada')
  console.log("### Elementos do form", email, img, fileUploader, "aqui");
  console.log("### DCOM Imagem em img.src", img.src);
  console.log("### DCOM Imagem em img_modificada.src", img_modificada.src);
// Data
let file = {};

// File Uploaded Change Event
fileUploader.addEventListener('change', function (e) {
  file = e.target.files[0];
})


// Signup User
function signUpUser() {  // Create user
  firebase.auth().createUserWithEmailAndPassword(email.value, pword.value).then(auth => {

    // Upload A Profile Image to the Cloud Storage
    firebase
      .storage()
      .ref("users")
      .child(auth.user.uid + "/profile.jpg")
      .put(file);

  }).catch(error => {
    console.log("### Autenticacao:", error.message)

  })

}

// https://medium.com/codingurukul/firebase-for-web-authentication-auth-with-email-and-password-cc4f7b4efc1b
function signInUser() { 
  firebase.auth().signInWithEmailAndPassword(email.value, pword.value).then(auth => {
      // Upload A Profile Image to the Cloud Storage
      console.log("### SignInUser: Upload do arquivo",file," para usuario",email.value, " id",auth.user.uid)
      userid = auth.user.uid;
      firebase
      .storage()
      .ref("users")
      .child(auth.user.uid + "/profile.jpg")
      .put(file);
  }).catch(function(error) {
    // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log("Erro em singInUser para", email.value)
      console.log(errorCode);
      console.log(errorMessage);
    });  

  }
// https://medium.com/codingurukul/firebase-for-web-authentication-auth-with-email-and-password-cc4f7b4efc1b

// Check to see if a user is logged in or not
firebase.auth().onAuthStateChanged(user => {

  if (user) {

    // Get A Profile Image from the Cloud Storage
    firebase
      .storage()
      .ref("users")
      .child(user.uid + "/profile.jpg")
      .getDownloadURL()
      .then(imgUrl => {
        img.src = imgUrl;
        img_original_URL = imgUrl;

        console.log("### Imagem URL", img.src);
      });
      //
      console.log("### NAO MAIS Gravando email do usuario",email.value);
      //// firebase.storage().ref("users/" + user.uid).update({Email: email.value});
      //
  }
})

async function show() {
  console.log(" ### Show(): ", "users/" + userid + "/imagem");
  var database = firebase.database();  // senão, database is not defined
  
  // Associa as variáveis abaixo aos elementos <img_id> no HTML
  img_modificada =  document.getElementById('img_modificada');  
  img_original  =   document.getElementById('img'); // Acrescentado 2023-02-01

  // var name="Meu teste";
  // firebase.database().ref("users/" + userid).set({name:name});  //Grava e .on('value',... pega

   firebase.database().ref("users/" + userid).on('value',(snap)=>{
    console.log("### SNAP val", snap.val());
    console.log("### SNAP imagem modif", snap.val().imagem);
    console.log("### Imagem original", img_original_URL);
 
    // img_modificada.src=snap.val().imagem + "?aleat=" + Date.now();   //Sugestão Guto para bypassar cache
    
    img_modificada.src=snap.val().imagem;  
    temp = img_modificada.src;
    img_modificada.src = temp + "&aleatorio=" + Date.now();
    console.log("### Imagem modif para o GET no html: ", img_modificada.src);

    img_original.src=img_original_URL + ", " + Date.now();


  });

}
